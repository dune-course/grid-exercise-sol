// DUNE Workshop Feb 2015

// exercise2.cc
// Implementation of the explicit finite volume method
// for the transport equation

#include "config.h"
#include <iostream>
#include <sstream>
#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
//#include <dune/grid/io/file/dgfparser.hh>
#include "fv.hh"

// Class encapsulating the equation data
// The velocity field is constant in dircetion (1, 1).  There is no
// inflow from the boundary.
template<class ctype, int dim, class Vars>
class SimpleTransport
{
public:
    // the velocity field
    Dune::FieldVector<double,dim>
    u(const Dune::FieldVector<ctype,dim>& global) const
    {
        Dune::FieldVector<double,dim> r(0.0);
        r[0] = 1.0;
        r[1] = 1.0;
        return r;
    }

    // Dirichlet boundary condition on inflow boundaries
    double
    c_in(const Dune::FieldVector<ctype,dim>& global) const
    {
        return 0.0;
    }
};

// initialize unknowns with initial value
template<class GV, class V>
void initialize (const GV& gridview, V& c)
{
    // first we extract the dimensions of the grid
    const int dimworld = GV::dimensionworld;

    // type used for coordinates in the grid
    typedef typename GV::ctype ct;

    // leaf iterator type
    typedef typename GV::template Codim<0>::Iterator Iterator;

    // create mapper
    Dune::MultipleCodimMultipleGeomTypeMapper<GV, FVLayout> mapper(gridview);

    // iterate through leaf grid an evaluate c0 at cell center
    Iterator endit = gridview.template end<0>();
    for (Iterator it = gridview.template begin<0>(); it!=endit; ++it)
    {
        // get global coordinate of cell center
        Dune::FieldVector<ct,dimworld> global = it->geometry().center();

        if (global[0]>=0.1 && global[0]<=0.2 &&
            global[1]>=0.1 && global[1]<=0.2)
            c[mapper.map(*it)] = 1.0;
        else
            c[mapper.map(*it)] = 0.0;
    }
}

int main(int argc, char** argv)
{
    try{
        Dune::MPIHelper::instance(argc, argv);

        // create the grid
        //typedef Dune::YaspGrid<2> GridType;
        //Dune::GridPtr<GridType> gridptr("unitsquare.dgf");
        //GridType& grid = *gridptr;

        // instantiate YaspGrid on the unit square with 100 by 100 cells:
        const int dim = 2;
        typedef Dune::YaspGrid<dim> GridType;
        Dune::FieldVector<double,dim> L(1.0);
        Dune::array<int,dim> N(Dune::fill_array<int,dim>(100));
        std::bitset<dim> periodic(false);
        GridType grid(L, N, periodic, 0);

        typedef GridType::LeafGridView GridView;
        GridView gridview = grid.leafGridView();

        // create concentration vector
        typedef std::vector<double> ScalarField;
        ScalarField c(gridview.size(0));

        // initialize concentration
        initialize(gridview, c);

        typedef SimpleTransport<double,GridView::dimension,ScalarField> Parameters;
        Parameters para;

        FiniteVolume<double,GridView,Parameters> fv(gridview, para);

        const double dt =  0.5*(L[0]/N[0]);
        double t = 0.0;
        for (int step = 0; step < 2 * N[0]; ++step)
        {
            // output solution:
            std::cout << "Solution at timestep " << step << ", time " << t << std::endl;

            // Instantiate the vtkwriter here
            Dune::VTKWriter<GridView> vtkwriter(gridview);

            // add cell data to vtk writer
            vtkwriter.addCellData(c, "c");

            // file output name
            std::ostringstream filename;
            filename << "c-" << std::setfill('0') << std::setw(4) << step;

            // use the write() method to write the data into the file
            // (get the filename using: filename.str().c_str())
            vtkwriter.write(filename.str().c_str(), Dune::VTK::appendedraw);

            // explicit update solution from t to t+dt
            fv.update(c,dt);

            // update time
            t += dt;
        }
    }
    catch (Dune::Exception &e){
        std::cerr << "Dune reported error: " << e << std::endl;
    }
    catch (...){
        std::cerr << "Unknown exception thrown!" << std::endl;
    }
}
