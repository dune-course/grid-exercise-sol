#ifndef FV_HH
#define FV_HH

#include <dune/common/fvector.hh>
#include <dune/grid/common/mcmgmapper.hh>

// Class encapsulating the equation data (velocity field, boundary conditions)
template<class ctype, class rtype, int dim>
class TransportEquationParameters
{
public:
    // the velocity field
    Dune::FieldVector<rtype,dim>
    u(const Dune::FieldVector<ctype,dim>& global) const;

    // Dirichlet boundary condition on inflow boundaries
    rtype
    c_in(const Dune::FieldVector<ctype,dim>& global) const;
};

// Layout class for the mapper
template<int dim>
struct FVLayout
{
    bool contains (Dune::GeometryType gt)
    {
        return gt.dim() == dim;
    }
};

// template parameters: GV = gridview type, E = equation parameters
template<class rtype, class GV, class E>
class FiniteVolume
{
public:
    //Grid dimension
    static const int dim = GV::dimension;
    static const int dimworld = GV::dimensionworld;

    //coordinate type
    typedef typename GV::ctype ctype;

    //solution vector
    typedef std::vector<rtype> ScalarField;

private:
    typedef typename GV::template Codim<0>::Iterator Iterator;
    typedef typename GV::IntersectionIterator IntersectionIterator;
    typedef typename GV::template Codim<1>::Entity::Geometry FaceGeometry;
    // mapper for elements (codim=0)
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GV,FVLayout> Mapper;

    const GV& gridview;
    const E& equation;
    Mapper mapper;
    ScalarField up;

public:
    FiniteVolume(const GV& gridview_, const E& equation_)
        : gridview(gridview_), equation(equation_), mapper(gridview_)
    {
        up.resize(gridview.size(0),false);
    }

    void update(ScalarField& c, rtype dt);
};

template<class rtype, class GV, class E>
void FiniteVolume<rtype,GV,E>::update(ScalarField& c, rtype dt)
{
    // initialize update vector
    for (unsigned i = 0; i < up.size(); ++i)
        up[i] = 0.0;

    const Iterator itend = gridview.template end<0>();
    for (Iterator it = gridview.template begin<0>(); it != itend; ++it)
    {
        const int cell_index = mapper.map(*it);

        // cell volume
        double volume = it->geometry().volume();

        // convection term -- boundary integral
        IntersectionIterator isend = gridview.iend(*it);
        for (IntersectionIterator is = gridview.ibegin(*it); is != isend; ++is)
        {
            const FaceGeometry& fgeo = is->geometry();

            // get the global coordinates of the face's center
            const Dune::FieldVector<ctype,dim> center = fgeo.center();
            // get the outer normal vector at the face center
            const Dune::FieldVector<ctype,dim>
                normal = is->centerUnitOuterNormal();
            // get the velocity vector at the face center
            const Dune::FieldVector<rtype,dim> u = equation.u(center);

            // compute face integral
            rtype flux = (u * normal) * fgeo.volume()/volume;

            // handle interior face
            if (is->neighbor())
            {
                int outside_index = mapper.map(*is->outside());

                if (flux > 0.0)           // upwind decision
                    // outflow
                    up[cell_index] -= c[cell_index]*flux;
                else
                    // inflow
                    up[cell_index] -= c[outside_index]*flux;
            }

            // handle boundary face
            if (is->boundary())
            {
                if (flux > 0.0)           // Inflow or outflow?
                    // outflow
                    up[cell_index] -= c[cell_index]*flux;
                else
                    // inflow
                    up[cell_index] -= equation.c_in(center)*flux;
            }
        }
    }

    // update solution
    for (unsigned int i=0; i<c.size(); ++i)
        c[i] += dt*up[i];
}

#endif
